package com.example.dmitrys.myapplication2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Button
import android.view.View
import android.os.CountDownTimer

class MainActivity : AppCompatActivity() {

    private val millisPerSecond: Long = 1000
    private val secondsToCountdown: Long = 30

    private var countdownDisplay: TextView? = null
    private var timer: CountDownTimer? = null
    private var startButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        countdownDisplay = findViewById(R.id.display_box)
        startButton = findViewById(R.id.startbutton)

        val listener = View.OnClickListener{
                try {
                    showTimer(secondsToCountdown * millisPerSecond)
                } catch (e: NumberFormatException) {
                    // method ignores invalid (non-integer) input and waits
                    // for something it can use
                }
            }
        startButton?.setOnClickListener(listener)
    }

    private fun showTimer(countdownMillis: Long) {
        timer?.cancel()
        timer = object : CountDownTimer(countdownMillis, millisPerSecond) {
            override fun onTick(millisUntilFinished: Long) {
                countdownDisplay?.text =
                        String.format(
                            getString(R.string.counting),
                            millisUntilFinished / millisPerSecond)
            }

            override fun onFinish() {
                countdownDisplay?.setText(R.string.kaboom)
            }
        }.start()
    }
}
